[TOC]

**解决方案介绍**
===============
该解决方案能帮您快速在华为云上部署高可用的云上网站架构，支持业务流量跨可用区进行分发，并具备跨可用区故障容灾的能力。适用于如下场景：

1、云上搭建电商平台。

2、门户网站、论坛、博客等高可用网站场景。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/high-availability-website-architecture.html

**架构图**
---------------
![方案架构](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/high-availability-website-architecture/readme/high-availability-website-architecture.png)

**架构描述**
---------------
该解决方案会部署如下资源：
1. 创建2台弹性云服务器ECS部署在不同可用区，用于对外提供服务。
2. 部署1个弹性公网EIP并关联弹性负载均衡ELB，用于接收网络流量。
3. 部署1个弹性负载均衡ELB，用于业务流量跨可用区进行分发。
4. 云数据库RDS实例，主备分区部署，具备跨可用区故障容灾的能力。

**组织结构**
---------------

``` lua
huaweicloud-solution-high-availability-website-architecture
├── high-available-web-min.tf.json -- 资源编排模板
```
**开始使用**
---------------
1、重置密码

登录[华为云服务器控制台](https://console.huaweicloud.com/ecm/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)，区域选择“北京四”，参考官网[重置弹性云服务器密码](https://support.huaweicloud.com/usermanual-ecs/zh-cn_topic_0067909751.html)，修改弹性云服务器初始化密码。

2、查看负载均衡实例

在[弹性负载均衡ELB控制台](https://console.huaweicloud.com/vpc/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&region=cn-north-4&locale=zh-cn#/elb/list)，查看该方案一键部署创建的ELB实例及其绑定的弹性公网IP。

图1 ELB实例
![ELB实例](
https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/high-availability-website-architecture/readme/readme-image-001.png)

3、查看弹性云服务器实例

在[弹性云服务器控制台](https://console.huaweicloud.com/ecm/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)，查看该方案一键部署创建的弹性云服务器ECS实例。

图2 ECS实例
![ECS实例](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/high-availability-website-architecture/readme/readme-image-002.png)

4、查看数据库实例

在[云数据库RDS控制台](https://console.huaweicloud.com/rds/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&region=cn-north-4&locale=zh-cn#/rds/management/list)，查看该方案一键部署生成的数据库实例。

图3 RDS实例
![RDS实例](
https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/high-availability-website-architecture/readme/readme-image-003.png)